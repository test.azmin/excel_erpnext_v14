# syntax=docker/dockerfile:1.3

ARG ERPNEXT_VERSION
FROM frappe/erpnext-worker:${ERPNEXT_VERSION}

COPY repos ../apps

USER root

RUN install-app whitelabel && \
    install-app healthcare && \
    install-app hospitality && \
    install-app non_profit && \
    install-app education && \
    install-app agriculture && \
    install-app hrms && \
    install-app ecommerce_integrations && \
    install-app excel_erpnext

USER frappe
