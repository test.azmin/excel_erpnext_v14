# !/bin/bash

# Use PAT or ssh for private repos.

## To use SSH keys place the private key as ssh-keys/ci and uncomment following
# mkdir -p repos ~/.ssh
# chown 0600 ssh-keys/ci
# chown 0700 ~/.ssh
# ssh-keyscan -t rsa git.example.com >> ~/.ssh/known_hosts
# eval $(ssh-agent -s)
# ssh-add ssh-keys/ci

## To clone private repos through ssh use following instead of https.
# git clone --depth 1 --branch master ssh://git@github.com/frappe/twilio-integration repos/twilio_integration

## To use PAT use PERSONAL_ACCESS_TOKEN environment variable which is set through CI secret.
# git clone --depth 1 --branch master https://username:${PERSONAL_ACCESS_TOKEN}@github.com/frappe/twilio-integration repos/twilio_integration

# Following are public repositories.
git clone --depth 1 --branch develop https://github.com/frappe/education.git repos/education
git clone --depth 1 --branch version-14 https://github.com/frappe/health.git repos/healthcare
git clone --depth 1 --branch develop https://github.com/frappe/hospitality.git repos/hospitality
git clone --depth 1 --branch develop https://github.com/frappe/agriculture.git repos/agriculture
git clone --depth 1 --branch  version-14 https://github.com/frappe/hrms.git repos/hrms
git clone --depth 1 --branch version-13 https://gitlab.com/test.azmin/excel_erpnext.git repos/excel_erpnext
git clone --depth 1 --branch develop https://github.com/frappe/non_profit.git repos/non_profit
git clone --depth 1 --branch main https://github.com/frappe/ecommerce_integrations.git repos/ecommerce_integrations
git clone --depth 1 --branch main https://gitlab.com/excel.azmin/whitelabel.git repos/whitelabel
